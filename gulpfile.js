const // development mode
    dev = false,
    // modules
    gulp = require('gulp'),
    newer = require('gulp-newer'),
    htmlclean = require('gulp-htmlclean'),
    noop = require('gulp-noop'),
    terser = require('gulp-terser'),
    rename = require('gulp-rename'),
    concate = require('gulp-concat'),
    sync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    babel = require('gulp-babel'),
    // folders
    src = 'src/',
    public = 'public/';

// HTML processing
function html() {
    const input = src + 'html/**/*';
    const output = public;

    return gulp
        .src(src + 'html/**/*')
        .pipe(newer(output))
        .pipe(dev ? noop() : htmlclean())
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// CSS processing
function css() {
    const input = src + 'scss/main.scss';
    const output = public + 'css';

    return gulp
        .src(input)
        .pipe(newer(output))
        .pipe(
            sass({
                errLogToConsole: dev,
                outputStyle: dev ? 'expanded' : 'compressed',
            }).on('error', sass.logError)
        )
        .pipe(
            rename({
                suffix: '.min',
            })
        )
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// JS processing
function js() {
    const input = [
        src + 'js/base/**/*',
        src + 'js/components/**/*',
        src + 'js/pages/**/*',
        src + 'js/main.js',
    ];
    const output = public + 'js';

    return gulp
        .src(input)
        .pipe(concate('app.js'))
        .pipe(
            dev
                ? noop()
                : babel({
                      presets: ['@babel/preset-env'],
                      plugins: [
                          '@babel/plugin-syntax-class-properties',
                          'transform-class-properties',
                      ],
                  })
        )
        .pipe(dev ? noop() : terser())
        .pipe(
            rename({
                suffix: '.min',
            })
        )
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// watch for file changes
function watch(done) {
    sync.init({
        server: {
            baseDir: './' + public,
        },
    });

    sync.reload();

    // html changes
    gulp.watch(src + 'html/**/*', html);

    // // scss changes
    gulp.watch(src + 'scss/**/*', css);

    // js changes
    gulp.watch(src + 'js/**/*', js);

    done();
}

// Single tasks
// exports.html = html;
exports.html = html;
exports.css = css;
exports.js = js;

exports.watch = watch;

// run all build tasks
exports.build = gulp.parallel(exports.html, exports.css, exports.js);

// default task
exports.default = gulp.series(exports.build, exports.watch);
