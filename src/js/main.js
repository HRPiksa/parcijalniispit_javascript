/*  Application Controller
---------------------------------------------------- */

const App = (() => {
    'use strict';

    return {
        init: function (options) {
            this.initBase(options);
            this.initComponents(options);
            this.initPages(options);
        },
        initBase: (options) => {},
        initComponents: (options) => {},
        initPages: (options) => {
            handleItunesSearch(options);
        },
    };
})();
