class Itunes {
    constructor(options) {
        this.inputTask = document.querySelector(options.itunesSearch.inputTask);
        this.searchItem = document.querySelector(
            options.itunesSearch.searchItem
        );
        this.resetItems = document.querySelector(
            options.itunesSearch.resetItems
        );
        this.songsTable = document.querySelector(
            options.itunesSearch.songsTable
        );
        this.loader = document.querySelector(options.itunesSearch.loader);
        this.endpoint = options.itunesSearch.endpoint;
        this.cors = options.itunesSearch.cors;

        this.popup = {
            backdrop: document.querySelector(
                options.itunesSearch.popup.backdrop
            ),
            popup: document.querySelector(options.itunesSearch.popup.popup),
            popupHeader: document.querySelector(
                options.itunesSearch.popup.popupHeader
            ),
            popupBody: document.querySelector(
                options.itunesSearch.popup.popupBody
            ),
            popupHeaderText: document.querySelector(
                options.itunesSearch.popup.popupHeaderText
            ),
            closeBtn: document.querySelector(
                options.itunesSearch.popup.closeBtn
            ),
        };
    }

    soundPlayer = {
        audio: null,
        playing: false,
        _ppromis: null,
        playedButton: null,
        pause: function () {
            this._ppromis = this.audio.pause();
            this.playing = false;

            this.playedButton.style.background = 'lightblue';
            this.playedButton.innerHTML = 'Play';
            this.elementsDisable(false);
        },

        elementsDisable: function (req) {
            searchItem.disabled = req;
            resetItems.disabled = req;
            inputTask.disabled = req;
        },

        play: function (file) {
            if (!this.audio && this.playing === false) {
                this.audio = new Audio(file);
                this.audio.onended = () => {
                    this.playedButton.style.background = 'lightblue';
                    this.playedButton.innerHTML = 'Play';
                    this.elementsDisable(false);
                };
                this._ppromis = this.audio.play();
                this.playing = true;

                this.playedButton.style.background = 'forestgreen';
                this.playedButton.innerHTML = 'Stop';
                this.elementsDisable(true);
            } else if (!this.playing) {
                this.playing = true;
                this.audio.src = file;
                this._ppromis = this.audio.play();

                this.playedButton.style.background = 'forestgreen';
                this.playedButton.innerHTML = 'Stop';
                this.elementsDisable(true);
            }
        },
    };

    removeRows = (table) => {
        let tblBody = table.querySelector('tbody');
        while (tblBody.lastElementChild) {
            tblBody.removeChild(tblBody.lastElementChild);
        }
    };

    errorHandler = (statusCode) => {
        console.log('Faild with status: ', statusCode);
        this.showLoader(this.loader, false);
        this.showPopup(`Faild with status: ${statusCode}`, 'ERROR');
    };

    playSong = (event) => {
        if (!this.soundPlayer.playing) {
            let url = event.target.dataset.previewUrl;
            let playedButton = event.target;
            this.soundPlayer.playedButton = playedButton;
            this.soundPlayer.play(url);
        } else {
            this.soundPlayer.pause();
        }
    };

    addTableRow = (tblBody, dataObj, cnt) => {
        const tr = document.createElement('tr');

        if (cnt > 0) {
            const td1 = document.createElement('td');
            td1.innerHTML = dataObj.artistName;
            td1.dataset.previewUrl = dataObj.previewUrl;
            tr.appendChild(td1);

            const td2 = document.createElement('td');
            td2.innerHTML = dataObj.trackName;
            td2.dataset.previewUrl = dataObj.previewUrl;
            tr.appendChild(td2);

            const btn = document.createElement('button');
            btn.dataset.previewUrl = dataObj.previewUrl;
            btn.innerHTML = 'Play';
            btn.classList.add('cell-button');
            btn.addEventListener('click', this.playSong);
            tr.appendChild(btn);
        } else {
            const td1 = document.createElement('td');
            td1.innerHTML = 'Nema podataka';
            td1.dataset.previewUrl = '';
            td1.setAttribute('colspan', '3');
            tr.appendChild(td1);
        }

        tblBody[0].appendChild(tr);
    };

    processSearchSongs = (data) => {
        setTimeout(() => this.showLoader(this.loader, false), 2000);

        const tbody = this.songsTable.getElementsByTagName('tbody');
        let resultCount = data.resultCount;
        if (resultCount > 0) {
            data.results.forEach((element) => {
                let dataObj = {};
                dataObj['artistName'] = element.artistName;
                dataObj['trackName'] = element.trackName;
                dataObj['previewUrl'] = element.previewUrl;

                this.addTableRow(tbody, dataObj, resultCount);
            });

            this.showPopup(`Podaci su uspješno dohvaćeni`, 'SUCCESS');
            setTimeout(() => {
                this.popup.closeBtn.click();
            }, 2000);
        } else {
            this.addTableRow(tbody, null, 0);

            this.showPopup(`Nema podataka za prikaz`, 'INFO');
        }
    };

    makeAjaxCall = (endpoint, term, cors) => {
        const params = `?term=${term}&entity=song`;
        const completeURL = `${cors}${endpoint}${params}`;
        const method = 'GET';

        let promise = new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(method, completeURL, true);

            this.showLoader(this.loader, true);

            xhr.send();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        console.log('XHR done successfully!');
                        let response = JSON.parse(xhr.responseText);
                        resolve(response);
                    } else {
                        reject(xhr.status + '   ' + xhr.statusText);
                        console.log(`XHR faild:  ${xhr.statusText}`);
                    }
                } else {
                    this.showLoader(this.loader, false);
                    console.log(
                        `XHR processing ready state was stopped at: ${xhr.readyState}`
                    );
                }
            };
        });

        return promise;
    };

    showLoader = (element, reload) => {
        reload
            ? element.classList.add('loader')
            : element.classList.remove('loader');
    };

    addSongsItem = () => {
        let text = this.inputTask.value;
        if (!text) {
            //alert('Upiši tekst za pretragu');
            this.showPopup(`Upišite tekst za pretragu`, 'WARNING');
            this.inputTask.focus();
            return;
        }
        this.popup.closeBtn.click();

        this.removeRows(this.songsTable);

        this.makeAjaxCall(this.endpoint, text, this.cors).then(
            this.processSearchSongs,
            this.errorHandler
        );

        inputTask.value = '';
    };

    searchSongsItems = () => {
        this.popup.closeBtn.click();

        let text = this.inputTask.value;
        if (text) {
            this.removeRows(this.songsTable);

            this.makeAjaxCall(this.endpoint, text, this.cors).then(
                this.processSearchSongs,
                this.errorHandler
            );
        }
    };

    resetSongsItems = () => {
        this.popup.closeBtn.click();

        inputTask.value = '';

        this.removeRows(this.songsTable);
    };

    showPopup = (text, title) => {
        while (this.popup.popupBody.firstChild) {
            this.popup.popupBody.removeChild(this.popup.popupBody.firstChild);
        }
        this.popup.popup.style.display = 'block';
        this.popup.backdrop.style.display = 'block';
        this.popup.popupHeader.classList.remove(
            ...this.popup.popupHeader.classList
        );
        switch (title.toUpperCase()) {
            case 'ERROR':
                this.popup.popupHeader.classList.add('popup-header', 'error');
                title = 'GREŠKA';
                break;
            case 'WARNING':
                this.popup.popupHeader.classList.add('popup-header', 'warning');
                title = 'UPOZORENJE';
                break;
            case 'INFO':
                this.popup.popupHeader.classList.add('popup-header', 'info');
                title = 'OBAVIJEST';
                break;
            case 'SUCCESS':
                this.popup.popupHeader.classList.add('popup-header', 'success');
                title = 'USPJEH';
                break;
            default:
                this.popup.popupHeader.classList.add('popup-header');
        }
        this.popup.popupHeaderText.innerHTML = title;
        this.popup.popupBody.innerHTML = text;
    };

    closePopup = () => {
        setTimeout(() => {
            this.popup.popup.style.display = 'none';
            this.popup.backdrop.style.display = 'none';
        }, 300);
    };

    addListeners() {
        //this.inputTask.addEventListener('keyup', this.searchSongsItems);
        this.searchItem.addEventListener('click', this.addSongsItem);
        this.resetItems.addEventListener('click', this.resetSongsItems);
        this.popup.closeBtn.addEventListener('click', this.closePopup);
    }

    init() {
        this.addListeners();
    }
}
